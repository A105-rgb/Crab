
add_namespace = italy_debug

# СССР объявляет войну всей Европе
 country_event = {
  id = italy_debug.1
  title = italy_debug.1.t
  desc = italy_debug.1.d
  picture = GFX_report_event_empire_ultimatum_crab
  
  is_triggered_only = yes
  
  option = {
	name = italy_debug.1.a
	
	every_country = {
		limit = {
			OR = {
				capital_scope = {
					is_on_continent = europe
				}
				original_tag = TUR
			}
		}
		declare_war_on = {
			target = SOV
			type = annex_everything
		}
	}
  }
  
 }
 
  country_event = {
  id = italy_debug.2
  title = italy_debug.2.t
  desc = italy_debug.2.d
  picture = GFX_report_event_empire_ultimatum_crab
  
  is_triggered_only = yes
  
  option = {
	name = italy_debug.2.a
	
		
		launch_nuke = {
			province = 	6521
			state = 64
			controller = GER
			use_nuke = yes
		}
		
		launch_nuke = {
			
			state = 64
			controller = GER
			use_nuke = yes
		}
		
		launch_nuke = {
			province = 	6521
			state = 64
			controller = GER
			use_nuke = yes
		}
		
		launch_nuke = {
			province = 	6521
			state = 64
			controller = GER
			use_nuke = yes
		}
		
		launch_nuke = {
			province = 	6521
			state = 64
			controller = GER
			use_nuke = yes
		}
		
		launch_nuke = {
			province = 	6521
			state = 64
			controller = GER
			use_nuke = yes
		}
		
		launch_nuke = {
			province = 	6521
			state = 64
			controller = GER
			use_nuke = yes
		}
		
		launch_nuke = {
			province = 	6521
			state = 64
			controller = GER
			use_nuke = yes
		}
	
  }
  
 }
