add_namespace = ita_ger_pact

# Судьба пакта
  country_event = {
  id = ita_ger_pact.1
  title = ita_ger_pact.1.t
  picture = GFX_report_event_vienna_award_hungary
   desc = {
   text = ita_ger_pact.1.d_1
    trigger = {
	  GER = {
	   NOT = {
	   has_war_with = SOV
	   }
	  }
	}
   }
   desc = {
   text = ita_ger_pact.1.d_2
    trigger = {
	  GER = {
	   has_war_with = SOV
	  }
	}
   }
   
   is_triggered_only = yes
   
   option = {
    name = ita_ger_pact.1.a
	trigger = {
	 GER = {
	 has_war_with = SOV
	 }
	}
	ai_chance = {
	factor = 50
	}
	SOV = { 
	 country_event = { 
	 id = ita_ger_pact.2 
	 hours = 6 
	 } 
	}
	remove_ideas = italy_german_pact_of_crab_and_ariec
   }
   
   option = {
    name = ita_ger_pact.1.b
	ai_chance = {
	factor = 50
	}
	 if = {
	  limit = {
	   NOT = {
	   has_non_aggression_pact_with = GER
	   }
	  }
	  diplomatic_relation = {
      country = GER
      relation = non_aggression_pact
      active = yes
      }
	  GER = {
	  diplomatic_relation = {
      country = ITA
      relation = non_aggression_pact
      active = yes
      }
	  }
	   ITA = {
	    modify_timed_idea = {
        idea = italy_german_pact_of_crab_and_ariec
        days = 400
       }
	  }
	  set_rule = {
      can_declare_war_without_wargoal_when_in_war = no
      }
	  
	  set_global_flag = ita_ger_pact_saved
	  
	  else = {
	  set_rule = {
      can_declare_war_without_wargoal_when_in_war = no
      }
	  
	  ITA = {
	    modify_timed_idea = {
        idea = italy_german_pact_of_crab_and_ariec
        days = 400
       }
	  }
	  
	  set_global_flag = ita_ger_pact_saved
	  }
	 }
   }
   
   option = {
   name = ita_ger_pact.1.c
    trigger = {
	 GER = {
	  NOT = {
	  has_war_with = SOV
	  }
	 }
    }
	ai_chance = {
	factor = 50
	}
	create_wargoal = {
    type = annex_everything
    target = GER
    }
   }
  }
# Предложение Италии 
  country_event = {
  id = ita_ger_pact.2
  title = ita_ger_pact.2.t
  picture = GFX_report_event_aid_pact
  desc = ita_ger_pact.2.d
  
  is_triggered_only = yes
  
   option = {
   name = ita_ger_pact.2.a
    ai_chance = {
	factor = 60
	 modifier = {
	  surrender_progress > 0.2
	  factor = 100
	 }
	}
	ITA = { 
	 country_event = { 
	 id = ita_ger_pact.3 
	 hours = 6 
	 } 
	}
   }
   
   option = {
   name = ita_ger_pact.2.b
    ai_chance = {
	factor = 40
	}
   	ITA = { 
	 country_event = { 
	 id = ita_ger_pact.4 
	 hours = 6 
	 } 
	}
   }
  }
# Смертельный альянс 
  country_event = {
  id = ita_ger_pact.3
  title = ita_ger_pact.3.t
  desc = ita_ger_pact.3.d
  picture = GFX_report_event_aid_pact
  
  is_triggered_only = yes
  
   option = {
   name = ita_ger_pact.3.a
   add_ideas = deadly_alliance_idea 
   create_wargoal = {
    type = annex_everything
    target = GER
    }
	diplomatic_relation = {
	 country = SOV
	 relation = non_aggression_pact
	 active = yes
	 }
	hidden_effect = {
	 SOV = {
	  diplomatic_relation = {
	  country = ITA
	  relation = non_aggression_pact
	  active = yes
	  }
	  add_ideas = deadly_alliance_idea
	 }
	 set_global_flag = deadly_alliance_flag
	}
   }
  }
# Отказ Советов  
  country_event = {
  id = ita_ger_pact.4
  title = ita_ger_pact.4.t
  desc = ita_ger_pact.4.d
  picture = GFX_report_event_empire_ultimatum_crab
  
  is_triggered_only = yes
  
   option = {
    name = ita_ger_pact.4.a
    create_wargoal = {
	type = annex_everything
	target = GER
	}
   }
  }
  
  
  
  
#######################
###НОВОСТНОЙ ИВЕНТ
#######################
 # Смертельный Альянс новость
  news_event = {
   id = ita_ger_pact.99
   title = ita_ger_pact.99.t
   desc = ita_ger_pact.99.d
   picture = GFX_news_event_deadly_alliance
   major = yes
   
   trigger = {
   
	has_global_flag = deadly_alliance_flag
	ITA = { has_war_with = GER }
	
   }
   
   fire_only_once = yes
   
   option = {
   name = ita_ger_pact.99.a
    trigger = {
	 OR = {
	 TAG = ITA
	 TAG = SOV
	 is_in_faction_with = ITA
	 is_in_faction_with = SOV
	 }
	}
   }
   
   option = {
   name = ita_ger_pact.99.b
    trigger = {
	 OR = {
	 TAG = GER
	 is_in_faction_with = GER
	 }
	}
   }
   
   option = {
   name = ita_ger_pact.99.c
    trigger = {
	 NOT = {
	  OR = {
	  TAG = ITA
	  TAG = SOV
	  TAG = GER
	  is_in_faction_with = GER
	  is_in_faction_with = ITA
	  is_in_faction_with = SOV
	  }
	 }
	}
   }
  }