ideas = {

	country = {
		NATO_founder_idea = {
			
			removal_cost = -1
			allowed = { always = yes }
			
			picture = GFX_idea_ita_nato_founder
			
			modifier = {
				political_power_factor = 0.1
				democratic_drift = 0.10
				stability_factor = 0.10
			}
		}
		
		NATO_member_idea = {
			
			removal_cost = -1
			allowed = { always = yes }
			
			picture = GFX_idea_ita_nato_member_generic
			
			modifier = {
				political_power_factor = 0.05
				democratic_drift = 0.10
				stability_factor = 0.05
			}
		}
	}
	
}