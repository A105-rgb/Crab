ita_dem_battle_for_france_category = {
############################################################################################
########### РЕШЕНИЯ
############################################################################################
	ITA_dem_foreign_affairs_funding_small = {
		allowed = {
		}
		
		ai_will_do = {
			factor = 90
		}
		
		visible = {
#			OR = {
#				has_active_mission = ITA_dem_FRA_political_instability
#				has_active_mission = ITA_dem_FRA_french_capital
#				has_active_mission = ITA_dem_FRA_military_cooperation
#			}
		}
		
		available = {
			bff_foreign_affairs_funded_trigger = no		

			OR = {
				has_active_mission = ITA_dem_FRA_political_instability
				has_active_mission = ITA_dem_FRA_french_capital
				has_active_mission = ITA_dem_FRA_military_cooperation
			}
		}
		
		cost = 25
		
		complete_effect = {
			custom_effect_tooltip = ita_dem_foreign_affairs_funding_small_tt
			
			hidden_effect = {
				set_country_flag = {
					flag = ita_dem_foreign_affairs_funding_small_flag
					value = 1
				}
			}
		}
	}
	
	ITA_dem_foreign_affairs_funding_middle = {
		allowed = {
		}
		
		ai_will_do = {
			factor = 90
		}
		
		visible = {
		}
		
		available = {
			bff_foreign_affairs_funded_trigger = no		

			OR = {
				has_active_mission = ITA_dem_FRA_political_instability
				has_active_mission = ITA_dem_FRA_french_capital
				has_active_mission = ITA_dem_FRA_military_cooperation
			}
		}
		
		cost = 50
		
		complete_effect = {
			custom_effect_tooltip = ita_dem_foreign_affairs_funding_middle_tt
			
			hidden_effect = {
				set_country_flag = {
					flag = ita_dem_foreign_affairs_funding_middle_flag
					value = 1
				}
			}
		}
	}
	
	ITA_dem_foreign_affairs_funding_big = {
		allowed = {
		}
		
		ai_will_do = {
			factor = 90
		}
				
		visible = {
		}
		
		available = {
			bff_foreign_affairs_funded_trigger = no		

			OR = {
				has_active_mission = ITA_dem_FRA_political_instability
				has_active_mission = ITA_dem_FRA_french_capital
				has_active_mission = ITA_dem_FRA_military_cooperation
			}
		}
		
		cost = 100
		
		complete_effect = {
			custom_effect_tooltip = ita_dem_foreign_affairs_funding_big_tt
			hidden_effect = {
				set_country_flag = {
					flag = ita_dem_foreign_affairs_funding_big_flag
					value = 1
				}
			}	
		}
	}
	
	##### РЕШЕНИЯ ДЛЯ МИССИЙ #####
	
	ITA_dem_help_with_radicals = {
		allowed = {
		}
		
		available = {
			bff_foreign_affairs_funded_trigger = yes
		}
		
		visible = {
			has_active_mission = ITA_dem_FRA_political_instability
		}
		
		ai_will_do = {
			factor = 90
		}
		
		fire_only_once = yes
		
		complete_effect = {
		}
		
		days_remove = 10
		
		remove_effect = {
			if = {
				limit = {
					has_country_flag = ita_dem_foreign_affairs_funding_small_flag
				}
				
				FRA = {
					country_event = {
						id = bff_ita.1
					}
				}
				
				else_if = {
					limit = {
						has_country_flag = ita_dem_foreign_affairs_funding_middle_flag
					}
					
					FRA = {
						country_event = {
							id = bff_ita.2
						}
					}
				}
				
				else = {
					FRA = {
						country_event = {
							id = bff_ita.3
						}
					}
				}
			}
		}
	}
	
	ITA_dem_contact_with_french_capital = {
		allowed = {
		}
		
		available = {
			bff_foreign_affairs_funded_trigger = yes
		}
		
		visible = {
			has_active_mission = ITA_dem_FRA_french_capital
		}
		
		ai_will_do = {
			factor = 90
		}
		
		fire_only_once = yes
		
		complete_effect = {
		}
		
		days_remove = 10
		
		remove_effect = {
			if = {
				limit = {
					has_country_flag = ita_dem_foreign_affairs_funding_small_flag
				}
				
				FRA = {
					country_event = {
						id = bff_ita.6
					}
				}
				
				else_if = {
					limit = {
						has_country_flag = ita_dem_foreign_affairs_funding_middle_flag
					}
					
					FRA = {
						country_event = {
							id = bff_ita.7
						}
					}
				}
				
				else = {
					FRA = {
						country_event = {
							id = bff_ita.8
						}
					}
				}
			}
		}
	}
	
	ITA_dem_military_cooperation_negotiations = {
		allowed = {
		}
		
		available = {
			bff_foreign_affairs_funded_trigger = yes
		}
		
		visible = {
			has_active_mission = ITA_dem_FRA_military_cooperation
		}
		
		ai_will_do = {
			factor = 90
		}
		
		fire_only_once = yes
		
		complete_effect = {
		}
		
		days_remove = 10
		
		remove_effect = {
			if = {
				limit = {
					has_country_flag = ita_dem_foreign_affairs_funding_small_flag
				}
				
				FRA = {
					country_event = {
						id = bff_ita.10
					}
				}
				
				else_if = {
					limit = {
						has_country_flag = ita_dem_foreign_affairs_funding_middle_flag
					}
					
					FRA = {
						country_event = {
							id = bff_ita.11
						}
					}
				}
				
				else = {
					FRA = {
						country_event = {
							id = bff_ita.12
						}
					}
				}
			}
		}
	}
	
	ITA_dem_mission_in_paris_struggle = {
		allowed = {
		}
		
		available = {
		}
		
		visible = {
			has_active_mission = ITA_dem_FRA_choosing_of_alliance
		}
		
		ai_will_do = {
			factor = 90
		}		
		
		cost = 15
		
		complete_effect = {
		}
		
		days_remove = 10
		
		remove_effect = {
			custom_effect_tooltip = ita_dem_foreign_affairs_funding_small_tt
			
			hidden_effect = {
				add_to_variable = {
					var = ROOT.ita_battle_for_france_influence_value
					value = 5
				}
			}
		}
	}
	
############################################################################################
########### МИССИИ
############################################################################################

	ITA_dem_FRA_political_instability = {
		
		icon = GFX_decision_generic_civil_support
		
		allowed = {
		}
		
		available = {
			bff_helped_with_the_radicals_trigger = yes
		}
		
		visible = {
			# has_global_flag = political_instability_in_france
		}
		
		activation = {
			always = no #has_global_flag = political_instability_in_france
		}
		
		days_mission_timeout = 30
		
		timeout_effect = {
			if = {
				limit = {
					original_tag = ITA
				}
				
				ENG = {
					custom_effect_tooltip = bff_timeout_effect_tt
					
					hidden_effect = {
						add_to_variable = {
							var = ita_battle_for_france_influence_value
							value = 10
						}
					}
				}
				
				else = {
					ITA = {
						custom_effect_tooltip = bff_timeout_effect_tt
						
						hidden_effect = {
							add_to_variable = {
								var = ita_battle_for_france_influence_value
								value = 10
							}
						}
					}
				}
			}
		}
		
		complete_effect = {
			effect_tooltip = {
				if = {
					limit = {
						has_country_flag = ita_dem_foreign_affairs_funding_small_flag
					}
					
					custom_effect_tooltip = ita_dem_foreign_affairs_funding_small_flag_tt
					
					else_if = {
						limit = {
							has_country_flag = ita_dem_foreign_affairs_funding_middle_flag
						}
						
						custom_effect_tooltip = ita_dem_foreign_affairs_funding_middle_tt
					}
					
					else_if = {
						limit = {
							has_country_flag = ita_dem_foreign_affairs_funding_big_flag
						}
						
						custom_effect_tooltip = ita_dem_foreign_affairs_funding_big_tt
					}
				}
			}
		}
	}
	
	ITA_dem_FRA_french_capital = {
		icon = GFX_decision_hol_attract_foreign_investors
		
		allowed = {
		}
		
		available = {
			bff_deal_with_french_capitalists_trigger = yes
		}
		
		visible = {
		}
		
		activation = {
			always = no
		}
		
		days_mission_timeout = 50
		
		timeout_effect = {
			if = {
				limit = {
					original_tag = ITA
				}
				
				ENG = {
					custom_effect_tooltip = bff_timeout_effect_tt
					
					hidden_effect = {
						add_to_variable = {
							var = ita_battle_for_france_influence_value
							value = 15
						}
					}
				}
				
				else = {
					ITA = {
						custom_effect_tooltip = bff_timeout_effect_tt
						
						hidden_effect = {
							add_to_variable = {
								var = ita_battle_for_france_influence_value
								value = 15
							}
						}
					}
				}
			}
		}
		
		complete_effect = {
			effect_tooltip = {
				if = {
					limit = {
						has_country_flag = ita_dem_foreign_affairs_funding_small_flag
					}
					
					custom_effect_tooltip = ita_dem_foreign_affairs_funding_small_flag_tt
					
					else_if = {
						limit = {
							has_country_flag = ita_dem_foreign_affairs_funding_middle_flag
						}
						
						custom_effect_tooltip = ita_dem_foreign_affairs_funding_middle_tt
					}
					
					else_if = {
						limit = {
							has_country_flag = ita_dem_foreign_affairs_funding_big_flag
						}
						
						custom_effect_tooltip = ita_dem_foreign_affairs_funding_big_tt
					}
				}
			}
		}
	}

	ITA_dem_FRA_military_cooperation = {
		icon = GFX_decision_generic_army_support
		
		allowed = {
		}
		
		available = {
			bff_military_cooperation_trigger = yes
		}
		
		visible = {
		}
		
		activation = {
			always = no
		}
		
		days_mission_timeout = 50
		
		timeout_effect = {
			if = {
				limit = {
					original_tag = ITA
				}
				
				ENG = {
					custom_effect_tooltip = bff_timeout_effect_tt
					
					hidden_effect = {
						add_to_variable = {
							var = ita_battle_for_france_influence_value
							value = 20
						}
					}
				}
				
				else = {
					ITA = {
						custom_effect_tooltip = bff_timeout_effect_tt
						
						hidden_effect = {
							add_to_variable = {
								var = ita_battle_for_france_influence_value
								value = 20
							}
						}
					}
				}
			}
		}
		
		complete_effect = {
			effect_tooltip = {
				if = {
					limit = {
						has_country_flag = ita_dem_foreign_affairs_funding_small_flag
					}
					
					custom_effect_tooltip = ita_dem_foreign_affairs_funding_small_flag_tt
					
					else_if = {
						limit = {
							has_country_flag = ita_dem_foreign_affairs_funding_middle_flag
						}
						
						custom_effect_tooltip = ita_dem_foreign_affairs_funding_middle_tt
					}
					
					else_if = {
						limit = {
							has_country_flag = ita_dem_foreign_affairs_funding_big_flag
						}
						
						custom_effect_tooltip = ita_dem_foreign_affairs_funding_big_tt
					}
				}
			}
		}
	}
	
	ITA_dem_FRA_choosing_of_alliance = {
		icon = GFX_decision_faction_tur_unaligned
		
		allowed = {
		}
		
		available = {
			always = no
		}
		
		visible = {
		}
		
		activation = {
			always = no
		}
		
		days_mission_timeout = 70
		
		timeout_effect = {
			if = {
				limit = {
					bff_ita_has_more_influence_trigger = yes
				}
				
				FRA = {
					country_event = {
						id = bff_ita.14
						hours = 2
					}
				}
				
				else_if = {
					limit = {
						bff_eng_has_more_influence_trigger = yes
					}
					
					FRA = {
						country_event = {
							id = bff_ita.15
							hours = 2
						}
					}
				}
			}
		}
		
		complete_effect = {
		}
	}
}