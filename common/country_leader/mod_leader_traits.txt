leader_traits = {
	
	crab_unionist = {
		random = no
		stability_factor = 0.15
		political_power_factor = 0.05
		war_support_factor = -0.05
		army_attack_factor = 0.25
		
		ai_will_do = {
			factor = 1
		}
	}
	
	controversial_leader = {
		random = no
		stability_factor = -0.05
		
		ai_will_do = {
			factor = 1
		}
	}
	
	incognito_mussolini = {
		communism_drift = 0.1
		drift_defence_factor = 0.25
		
		ai_will_do = {
			factor = 1
		}
	}
	
	mussolini_crazy = {
		random = no
		stability_factor = -0.10
		political_power_factor = 0.50
		war_support_factor = 0.2
		
		ai_will_do = {
			factor = 1
		}
	}
	
}