capital = 52

oob = "HGR_1939"

set_research_slots = 4
set_stability = 0.7
set_war_support = 0.8
add_ideas = {
}


# Starting tech

set_technology = {
		improved_light_tank = 1 
		basic_medium_tank = 1 
		CAS2 = 1
		heavy_fighter1 = 1
		interwar_artillery = 1
		interwar_antitank = 1
		infantry_weapons2 = 1
		improved_infantry_weapons = 1
		support_weapons = 1
		support_weapons2 = 1
		paratroopers = 1
		

		#doctrines
		delay = 1
		elastic_defence = 1
		armored_spearhead = 1
		schwerpunk = 1
		blitzkrieg = 1
		convoy_interdiction_ti = 1
		unrestricted_submarine_warfare = 1
		raider_patrols = 1
		#air
		formation_flying = 1		
		dive_bombing = 1
		direct_ground_support = 1

		#electronics
		electronic_mechanical_engineering = 1
		radio = 1
		radio_detection = 1
		decimetric_radar = 1
		mechanical_computing = 1
		computing_machine = 1
		basic_encryption = 1
		basic_decryption = 1

		#industry
		basic_machine_tools = 1
		improved_machine_tools = 1
		advanced_machine_tools = 1
		oil_processing = 1
		fuel_refining2 = 1
		construction1 = 1
		construction2 = 1
		construction3 = 1
		construction4 = 1
		dispersed_industry = 1
		dispersed_industry2 = 1
		dispersed_industry3 = 1
		dispersed_industry4 = 1

	}
	if = {
		limit = {
			not = {
				has_dlc = "Man the Guns"
			}
		}
		set_technology = {
			basic_heavy_cruiser = 1
			improved_heavy_cruiser = 1
			improved_battleship = 1
			early_carrier = 1
			basic_carrier = 1
			improved_carrier = 1
		}
		set_naval_oob = "GER_1939_naval_legacy"
	}
	if = {
		limit = {
			has_dlc = "Man the Guns"
		}
		set_technology = {
			improved_ship_hull_cruiser = 1
			basic_ship_hull_heavy = 1
			early_ship_hull_carrier = 1
			basic_ship_hull_carrier = 1
			improved_heavy_battery = 1
			basic_heavy_armor_scheme = 1
		}
		set_naval_oob = "GER_1939_naval_mtg"
	}
	
set_convoys = 0

set_politics = {
	ruling_party = neutrality
	last_election = "1934.3.26"
	election_frequency = 60
	elections_allowed = no
}
set_popularities = {
	neutrality = 60
	fascism = 25
	communism = 5
	democratic = 10
}

create_country_leader = {
	name = "Heinrich Himmler"
	desc = "POLITICS_HEINRICH_HIMMLER_DESC"
	picture = "Portrait_Germany_Heinrich_Himmler.dds"
	expire = "1965.1.1"
	ideology = cultism
	traits = {
		
	}
}